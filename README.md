# 《Academic Express 学术快递》微信小程序
## 文件结构
- icon/: 存储应用的图标
- image/: 存储应用中使用的图片资源，包括首页轮播图、导航图、论文封面图和人物肖像图
- pages/: 存储应用的页面相关文件，包括wxml、wxss、js等，用于定义应用的界面和交互逻辑。
- utils/: 存储应用中使用的工具代码，如网络请求、数据处理等。
- app.js: 是整个小程序的逻辑入口文件。
- app.json: 定义了应用的公共设置，如应用的名称、描述、版本号、作者等。
- project.config.json: 存储项目的配置信息，如编译器版本、是否使用cdn等。
- project.private.config.json: 存储项目的私有配置信息，如服务器地址、端口号等。
- readme.md: 项目的说明文档，一般用来自我介绍、功能说明、使用方法等。
- sitemap.json: 定义了应用的访问路径和页面路由。

## 微信小程序的页面
微信小程序所展示的每一个页面都需要有对应的一组文件(.js, .json, .wxml, .wxss)进行描述。

- .wxml：
  - .wxml文件是微信小程序的标记语言，类似于HTML，用于描述页面的结构。
  - 它定义了页面中的各种元素和组件，如文本、图片、按钮、列表等。
  - .wxml文件中的标签和属性用于构建页面的视觉界面，但它们不包含任何逻辑。
- .wxss：
  - wxss（WeiXin Style Sheets）是微信小程序的样式表语言，类似于CSS。
  - 它用于定义和描述页面中元素的外观和样式，如颜色、字体、布局、间距等。
  - wxss文件包含了样式规则，这些规则可以继承自上级样式表，也可以覆盖或添加新的样式。
- .js：
  - .js文件是页面逻辑的载体，用于处理页面的交互逻辑。
  - 它类似于传统网页开发中的JavaScript，但是运行在微信的JavaScript环境中。
  - 在.js文件中，可以使用微信小程序提供的API进行数据请求、本地存储、位置信息获取等操作。
  - 页面的生命周期函数（如onLoad、onReady、onUnload等）也在.js文件中定义，用于处理页面的加载、显示和卸载等事件。
- .json:
  - json文件中的配置可以影响整个页面的外观和行为，但它们不处理具体的交互逻辑。
  - 它们通常与.wxml和.wxss文件配合使用，为页面提供全局的配置信息。

本项目的所有页面文件夹均存放于/pages文件夹下,同时 __在app.json文件中登记__。


```text
=> app.json

{
  "pages": [
    "pages/index/index",
    .
    .
  ]
  .
}

```
注意，虽然首页对应的页面文件夹称为index，但是在app.json中我们填写的地址需要具体到文件夹内部的文件名，由于index文件夹内.js,.json,.wxml,.wxss文件名相同（均为index）因此我们省略文件名后缀，统一用index代替。因此我们添加的地址为:/pages/index（页面文件夹名）/index（文件名，省略后缀）.

### WXML
在一个页面中，编辑.wxml即可实现内容的简单和列举。例如"pages/intro"页面（图形化界面请点击底部状态栏“关于”，再点击“小程序介绍”）。
这个页面没有丰富的页面动画和页面逻辑，仅仅是一个简单的页面打印。其对应的wxml代码和html代码非常类似：

```html
<view><image class='intro' mode = 'widthFix' src='/image/intro/nodel_prize_medal.jpeg'></image></view>

<view><title class = "title">小程序简介</title></view>
<view class= "str1">学术速递(Academic Express)由北京化工大学三名学生(熊圳、朱志樊、王宇)的选修课程项目设计，该小程序的开发旨在帮助大家快速便捷的浏览有关学科的重要文献内容，小程序的内容包括物理学、化学、生理学与数学的最新文献推送栏目，同时包含近年来诺贝尔奖得主的人物生平与主要贡献介绍。</view>

<view class="margin">·  简单易用的操作界面</view>
<view class="margin">·  图文并茂的文章内容</view>
<view class="margin">·  快捷方便的信息获取</view>

```
### WXSS

同样以"/pages/intro"举例，针对wxml编写的每一个标签的class属性，可以再定义具体的样式配置。
```css
/* pages/intro/intro.wxss */
.intro{
  width: 750rpx;
}

.title{
  font-size: 30rpx;
  font-weight: bold;
  justify-content: left;
}
.str1{
  text-indent: 80rpx;
  font-size: 25rpx;
  text-align: left;
  text-overflow:ellipsis;
  word-wrap:break-word;
  line-height: 25px
}
.margin{
  line-height: 45px
}
```
