// pages/article_list/article3/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var pdfUrl = "https://6f6e-onlinedata-2g0a9olrd594a4b5-1306563198.tcb.qcloud.la/articles/Detection_of_MLV-related_virus_gene_sequences.pdf?sign=3a770dced4ef6c08f9354ec50120facf&t=1626590977"
    wx.showLoading({
      title: '数据加载中'
    })
    wx.downloadFile({
      url: pdfUrl,
      success: function(t){
        wx.openDocument({
          filePath: t.tempFilePath,
        })
      },
      fail: function(t){

      },
      complete: function(t){
        wx.hideLoading()
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})