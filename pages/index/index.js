// index.js
// 获取应用实例
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    
    article_list:[
      {
        id:0,
        name:"Nonlinear Gravitons and Curved Twistor Theory",
        discription:"Roger Penrose",
        url:"/pages/article_list/article1/index",
        cover: "/image/article_cover/grav.png"
      },
      {
        id:1,
        name:"Observational constraints on the formation and evolution of the Milky Way nuclear star cluster with Keck and Gemini",
        discription:"Andrea Ghez",
        cover:"/image/article_cover/grav.png",
        url:"/pages/article_list/article2/index"
      },
      {
        id:2,
        name:"The Galactic Center Massive Black Hole and Nuclear Star Cluster",
        discription:"Reinhard Genzel",
        cover:"/image/article_cover/grav.png",
        url:"/pages/article_list/article3/index"
      },
      {
        id:3,
        name:" An updated evolutionary classification of CRISPR–Cas systems",
        discription:"Emmanuelle Charpentier",
        cover:"/image/article_cover/grav.png",
        url:"/pages/article_list/article4/index"
      },
      {
        id:4,
        name:"Genomes in Focus: Development and Applications of CRISPR-Cas9 Imaging Technologies",
        discription:"Jennifer Doudna",
        cover:"/image/article_cover/grav.png",
        url:"/pages/article_list/article5/index"
      },
      {
        id:5,
        name:"Expanded Classification of Hepatitis C Virus Into 7 Genotypes and 67 Subtypes: Updated Criteria and Genotype Assignment Web Resource",
        discription:"Charles M. Rice",
        cover:"/image/article_cover/grav.png",
        url:"/pages/article_list/article6/index"
      },
      {
        id:6,
        name:"Detection of MLV-related virus gene sequences in blood of patients with chronic fatigue syndrome and healthy blood donors",
        discription:"Harvey J. Alter",
        cover:"/image/article_cover/grav.png",
        url:"/pages/article_list/article7/index"
      },
      {
        id:7,
        name:"Hepatitis C Virus–Specific Cytolytic T Lymphocyte and T Helper Cell Responses in Seronegative Persons",
        discription:"Michael Houghton",
        cover:"/image/article_cover/grav.png",
        url:"/pages/article_list/article8/index"
      },
      {
        id:8,
        name:"Sparse equidistribution problems, period bounds and subconvexity",
        discription:"Akshay Venkatesh",
        cover:"/image/article_cover/grav.png",
        url:"/pages/article_list/article9/index"
      },
      {
        id:9,
        name:"Existence and uniqueness of martingale solutions for SDEs with rough or degenerate coefficients",
        discription:"Alessio Figalli",
        cover:"/image/article_cover/grav.png",
        url:"/pages/article_list/article10/index"
      },
      {
        id:10,
        name:"Birational Geometry of algebraic varieties",
        discription:"Caucher Birkar",
        cover:"/image/article_cover/grav.png",
        url:"/pages/article_list/article11/index"
      },
      {
        id:11,
        name:"On torsion in the cohomology of locally symmetric varieties",
        discription:"Peter Scholze",
        cover:"/image/article_cover/grav.png",
        url:"/pages/article_list/article12/index"
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})