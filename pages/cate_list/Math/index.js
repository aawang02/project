// pages/cate_list/Physics/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    article_list:[
      {
        id:8,
        name:"Sparse equidistribution problems, period bounds and subconvexity",
        discription:"Akshay Venkatesh",
        cover:"/image/article_cover/grav.png",
        url:"/pages/article_list/article9/index"
      },
      {
        id:9,
        name:"Existence and uniqueness of martingale solutions for SDEs with rough or degenerate coefficients",
        discription:"Alessio Figalli",
        cover:"/image/article_cover/grav.png",
        url:"/pages/article_list/article10/index"
      },
      {
        id:10,
        name:"Birational Geometry of algebraic varieties",
        discription:"Caucher Birkar",
        cover:"/image/article_cover/grav.png",
        url:"/pages/article_list/article11/index"
      },
      {
        id:11,
        name:"On torsion in the cohomology of locally symmetric varieties",
        discription:"Peter Scholze",
        cover:"/image/article_cover/grav.png",
        url:"/pages/article_list/article12/index"
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})