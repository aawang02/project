// pages/cate_list/Physics/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    article_list:[
      {
        id:0,
        name:"Nonlinear Gravitons and Curved Twistor Theory",
        discription:"Roger Penrose",
        url:"/pages/article_list/article1/index",
        cover: "/image/article_cover/grav.png"
      },
      {
        id:1,
        name:"Observational constraints on the formation and evolution of the Milky Way nuclear star cluster with Keck and Gemini",
        discription:"Andrea Ghez",
        cover:"/image/article_cover/grav.png",
        url:"/pages/article_list/article2/index"
      },
      {
        id:2,
        name:"The Galactic Center Massive Black Hole and Nuclear Star Cluster",
        discription:"Reinhard Genzel",
        cover:"/image/article_cover/grav.png",
        url:"/pages/article_list/article3/index"
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})