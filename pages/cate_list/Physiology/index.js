// pages/cate_list/Physics/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    article_list:[
      {
        id:5,
        name:"Expanded Classification of Hepatitis C Virus Into 7 Genotypes and 67 Subtypes: Updated Criteria and Genotype Assignment Web Resource",
        discription:"Charles M. Rice",
        cover:"/image/article_cover/grav.png",
        url:"/pages/article_list/article6/index"
      },
      {
        id:6,
        name:"Detection of MLV-related virus gene sequences in blood of patients with chronic fatigue syndrome and healthy blood donors",
        discription:"Harvey J. Alter",
        cover:"/image/article_cover/grav.png",
        url:"/pages/article_list/article7/index"
      },
      {
        id:7,
        name:"Hepatitis C Virus–Specific Cytolytic T Lymphocyte and T Helper Cell Responses in Seronegative Persons",
        discription:"Michael Houghton",
        cover:"/image/article_cover/grav.png",
        url:"/pages/article_list/article8/index"
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})