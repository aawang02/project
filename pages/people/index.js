// pages/my2/my2.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    people_list:[
      {
        id:1,
        title: "2020诺贝尔物理学奖获得者",
        port: "/image/people/roger_penrose.png",
        name:"罗杰·彭罗斯",
        brief_disc : "罗杰·彭罗斯（Roger Penrose，1931年8月8日-），英国数学物理学家、牛津大学数学系名誉教授，他在数学物理方面的工作拥有高度评价，特别是对广义相对论与宇宙学方面的贡献。",
        page:"/pages/people_list/person1/index"
      },
      {
        id:2,
        title: "2020诺贝尔生理学或医学奖获得者",
        port: "/image/people/roger_penrose.png",
        name:"哈维·詹姆斯·阿尔特",
        brief_disc : "哈维·詹姆斯·奥尔特，美国医学研究员、病毒学家、医生和诺贝尔奖获得者，他以发现丙型肝炎病毒的工作而闻名。奥尔特因发现导致丙型肝炎的病毒而获得认可。他被授予杰出服务奖章，这是授予美国政府公共卫生部门平民的最高奖项，以及2000年阿尔伯特·拉斯克临床医学研究奖。",
        page:"/pages/people_list/person2/index"
      },
      {
        id:3,
        title:"2020诺贝尔化学奖获得者",
        port: "/image/people/roger_penrose.png",
        name:"埃马努埃尔·玛丽·夏彭蒂埃",
        brief_disc : "埃马努埃尔·玛丽·夏彭蒂埃，法国微生物学、遗传学和生物化学的教授和研究员。2020年，加州大学伯克利分校的查彭蒂埃和美国生物化学家詹妮弗·杜德纳因\"开发基因组编辑方法\"（通过CRISPR）获得诺贝尔化学奖。这是有史以来第一个由两位女性单独获得的科学诺贝尔奖。",
        page:"/pages/people_list/person3/index"
      },
      {
        id:4,
        title:"2020诺贝尔物理奖获得者",
        port: "/image/people/roger_penrose.png",
        name:"安德里亚·盖兹",
        brief_disc : "安德烈娅·盖兹（Andrea Ghez），1965年生于美国纽约市，美国天文学家，现为美国加利福尼亚大学洛杉矶分校教授，盖兹是迄今第四位获诺贝尔物理学奖的女性科学家。2020年10月6日，获得2020年诺贝尔物理学奖，因为在银河系中心发现了一个超大质量的致密天体。",
        page:"/pages/people_list/person4/index"
      },
      {
        id:5,
        title:"2020诺贝尔生理学或医学奖获得者",
        port: "/image/people/roger_penrose.png",
        name:"查尔斯·赖斯",
        brief_disc : "查尔斯·M·赖斯(Charles M. Rice),1952年出生于美国萨克拉门托，加州理工学院博士，美国病毒学家、医学家，洛克菲勒大学教授, 洛克菲勒大学病毒学和传染病实验室的负责人 ，主要研究领域是丙型肝炎病毒。 2020年凭借在“发现丙型肝炎病毒”方面作出的贡献，与哈维·阿尔特, 迈克尔·霍顿等人共同获得诺贝尔生理学或医学奖。",
        page:"/pages/people_list/person5/index"
      },
      {
        id:6,
        title:"2020诺贝尔物理学奖获得者",
        port: "/image/people/roger_penrose.png",
        name:"赖因哈德·根策尔",
        brief_disc : "赖因哈德·根策尔（Reinhard Genzel），1952年出生于德国法兰克福，1978年获得波恩大学博士学位。美国国家科学院院士，研究领域是天体物理学。现任慕尼黑马克斯-普朗克太空物理学研究所所长，美国加利福尼亚大学伯克利分校物理系教授。2020年10月6日，因在银河系中央发现超大质量天体获得2020年诺贝尔物理学奖。",
        page:"/pages/people_list/person6/index"
      },
      {
        id:7,
        title:"2020诺贝尔化学奖获得者",
        port: "/image/people/roger_penrose.png",
        name:"珍妮弗·道德纳",
        brief_disc : "珍妮弗·道德纳（Jennifer Doudna), 美国科学家，现为美国加州大学伯克利分校教授，霍华德·休斯医学研究所研究员。曾在科罗拉多大学博尔德分校在切赫实验室担任博士后，师从1989年诺贝尔化学奖得主托马斯·罗伯特·切赫>2020年10月7日，詹妮弗·杜德纳因“开发出一种基因组编辑方法”获得2020年诺贝尔化学奖。",
        page:"/pages/people_list/person7/index"
      },
      {
        id:8,
        title:"2020诺贝尔生理学或医学学奖获得者",
        port: "/image/people/roger_penrose.png",
        name:"迈克尔·霍顿",
        brief_disc : "迈克尔·霍顿（Michael Houghton），男，英国生物化学家，现任加拿大卓越研究员计划病毒学研究员、阿尔伯塔大学李嘉诚病毒学教授，并担任阿尔伯塔大学李嘉诚应用病毒学研究所主任。参与开发丙型肝炎测试。2020年10月5日，获得2020年诺贝尔生理学或医学奖，以表彰他“发现丙型肝炎病毒”方面作出的贡献。",
        page:"/pages/people_list/person8/index"
      },
      {
        id:9,
        title:"2018年菲尔兹奖获得者",
        port: "/image/people/roger_penrose.png",
        name:"阿克萨伊·文卡特什",
        brief_disc : "阿克萨伊·文卡特什，澳大利亚数学家, 1981年11月21日出生于印度新德里，在美国普林斯顿大学获得博士学位。目前是斯坦福大学的数学教授，研究领域主要是计数、自守形式的等分布问题以及数论，特别是表示论、局部对称空间以及遍历理论。2018年8月1日，由于其在解析数论、拓扑学、表示论等方面的综合成就，获颁菲尔兹奖。",
        page:"/pages/people_list/person9/index"
      },
      {
        id:10,
        title:"2018年菲尔兹奖获得者",
        port: "/image/people/roger_penrose.png",
        name:"阿莱西奥·菲加利",
        brief_disc : "阿莱西奥·菲加利，1984年出生于罗马（意大利）, 2007年从比萨高等师范学院和里昂高等师范学院获得联合博士学位。 2007年任命为国家科学研究中心（CNRS）的校长。2009年，他搬到德克萨斯大学奥斯汀分校担任副教授，2011年成为全学教授，2013年成为R.L.摩尔教授。2016年，他加入苏黎世ETH数学系。",
        page:"/pages/people_list/person10/index"
      },
      {
        id:11,
        title:"2018年菲尔兹奖获得者",
        port: "/image/people/roger_penrose.png",
        name:"考切尔·比尔卡尔",
        brief_disc : "考切尔·比尔卡尔（Caucher Birkar），1978年出生在伊朗西部库尔德地区的农村。曾就读伊朗德黑兰大学数学系，2004年获诺丁汉大学博士学位。毕业后迁居英国。主要研究领域是代数几何，特别是更高维度的双向几何。2018年荣获数学界最高奖菲尔兹奖; 现任清华大学丘成桐数学科学中心教授。",
        page:"/pages/people_list/person11/index"
      },
      {
        id:12,
        title:"2018年菲尔兹奖获得者",
        port: "/image/people/roger_penrose.png",
        name:"彼得·朔尔策",
        brief_disc : "彼得·朔尔策，于1987年12月11日出生在德国东部城市德累斯顿。现任德国波恩大学数学学院教授。2018年8月1日，年仅30岁的朔尔策获得菲尔茨奖，是该届菲尔茨奖的最年轻获得者。",
        page:"/pages/people_list/person12/index"
      }
    ]
  },
  // //lbe
  // onlbejsTap:function(){
  //   wx.navigateTo({
  //     url: '../my2/lbejs',
  //   })
  // },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})