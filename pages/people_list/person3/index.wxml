<text style="font-weight:bold">Emmanuelle Marie Charpentier</text>
<text class="text"> (born 11 December 1968[2]) is a French professor and researcher in microbiology, genetics, and biochemistry. Since 2015, she has been a Director at the Max Planck Institute for Infection Biology in Berlin. In 2018, she founded an independent research institute, the Max Planck Unit for the Science of Pathogens. In 2020, Charpentier and American biochemist Jennifer Doudna of the University of California, Berkeley, were awarded the Nobel Prize in Chemistry "for the development of a method for genome editing" (through CRISPR). This was the first science Nobel ever won by two women alone.</text>
<view class="tltle"> life and education</view>
<view class="text" >in 1968 in Juvisy-sur-Orge in France, Charpentier studied biochemistry, microbiology and genetics at the Pierre and Marie Curie University (today the Faculty of Science of Sorbonne University) in Paris. She was a graduate student at the Institut Pasteur from 1992 to 1995, and was awarded a research doctorate. Charpentier's PhD project investigated molecular mechanisms involved in antibiotic resistance.</view>
<view class="tltle">carrer and research</view>
<view class="text"> Max Planck Institute for Infection Biology in Berlin, Germany</view>
<view class="text"> worked as a university teaching assistant at Pierre and Marie Curie University from 1993 to 1995 and as a postdoctoral fellow at the Institut Pasteur from 1995 to 1996. She moved to the US and worked as a postdoctoral fellow at the Rockefeller University in New York from 1996 to 1997. During this time, Charpentier worked in the lab of microbiologist Elaine Tuomanen. Tuomanen's lab investigated how the pathogen Streptococcus pneumoniae utilizes mobile genetic elements to alter its genome. Charpentier also helped demonstrate how S. pneumoniae develop vancomycin resistance.</view>
<view class="text"> worked as an assistant research scientist at the New York University Medical Center from 1997 to 1999. There she worked in the lab of Pamela Cowin, a skin-cell biologist interested in mammalian gene manipulation. Charpentier published a paper exploring the regulation of hair growth in mice. She held the position of Research Associate at the St. Jude Children's Research Hospital and at the Skirball Institute of Biomolecular Medicine in New York from 1999 to 2002.</view>
<view class="text">After five years in the United States, Charpentier returned to Europe and became lab head and a guest professor at the Institute of Microbiology and Genetics, University of Vienna, from 2002 to 2004. In 2004, Charpentier published her discovery of an RNA molecule involved in the regulation of virulence-factor synthesis in Streptococcus pyogenes. From 2004 to 2006 she was lab head and an assistant professor at the Department of Microbiology and Immunobiology. In 2006 she became private docent (Microbiology) and received her habilitation at the Centre of Molecular Biology. From 2006 to 2009 she worked as lab head and Associate Professor at the Max F. Perutz Laboratories.</view>
<view class="text"> moved to Sweden and became lab head and associate professor at the Laboratory for Molecular Infection Medicine Sweden (MIMS), at Umeå University. She held the position as group leader from 2008 till 2013, and was Visiting Professor from 2014 to 2017. She moved to Germany to act as department head and W3 Professor at the Helmholtz Centre for Infection Research[15] in Braunschweig and the Hannover Medical School from 2013 until 2015. In 2014 she became an Alexander von Humboldt Professor.</view>
<view class="text">In 2015 Charpentier accepted an offer from the German Max Planck Society to become a scientific member of the society and a director at the Max Planck Institute for Infection Biology in Berlin. Since 2016, she has been a Honorary Professor at Humboldt University in Berlin, and since 2018, she is the Founding and Acting Director of the Max Planck Unit for the Science of Pathogens. Charpentier retained her position as Visiting Professor at Umeå University until the end of 2017, where a new donation from the Kempe Foundations and the Knut and Alice Wallenberg Foundation has given her the opportunity to offer more young researchers positions within research groups of the MIMS Laboratory.</view>
<view class="tltle">CRISPR/Cas9</view>
<view class="text"> is best known for her Nobel-winning work of deciphering the molecular mechanisms of a bacterial immune system, called CRISPR/Cas9, and repurposing it into a tool for genome editing. In particular, she uncovered a novel mechanism for the maturation of a non-coding RNA which is pivotal in the function of CRISPR/Cas9. Specifically, Charpentier demonstrated that a small RNA called tracrRNA is essential for the maturation of crRNA.</view>
<view class="text">In 2011, Charpentier met Jennifer Doudna at a research conference and they began a collaboration.Working with Jennifer Doudna's laboratory, Charpentier's laboratory showed that Cas9 could be used to make cuts in any DNA sequence desired. The method they developed involved the combination of Cas9 with easily created synthetic "guide RNA" molecules. Synthetic guide RNA is a chimera of crRNA and tracrRNA; therefore, this discovery demonstrated that the CRISPR-Cas9 technology could be used to edit the genome with relative ease. Researchers worldwide have employed this method successfully to edit the DNA sequences of plants, animals, and laboratory cell lines. CRISPR has revolutionized genetics by allowing scientists to edit genes to probe their role in health and disease and to develop genetic therapies with the hope that it will prove safer and more effective than the first generation of gene therapies.</view>
<view>In 2013, Charpentier co-founded CRISPR Therapeutics and ERS Genomics along with Shaun Foy and Rodger Novak.</view>
<view class="tltle">Awards</view>
<view class="text"> has been awarded numerous international prizes, awards, and acknowledgements, including the Nobel Prize in Chemistry, the Breakthrough Prize in Life Sciences, the Louis-Jeantet Prize for Medicine, the Gruber Foundation International Prize in Genetics, the Leibniz Prize, Germany's most prestigious research prize, the Tang Prize, the Japan Prize, and the Kavli Prize in Nanoscience. She has won the BBVA Foundation Frontiers of Knowledge Award jointly with Jennifer Doudna and Francisco Mojica, whose pioneering work has ignited "the revolution in biology permitted by CRISPR/Cas 9 techniques". These tools facilitate genome modification with an unprecedented degree of precision, and far more cheaply and straightforwardly than any previous method. Not unlike today's simple, intuitive word processing programs, CRISPR/Cas 9 is able to "edit" the genome by "cutting and pasting" DNA sequences: a technology so efficient and powerful that it has spread like wildfire round the laboratories of the world, explains the jury, "as a tool to understand gene function and treat disease". Also, in 2015, Time magazine designated Charpentier one of the 100 most influential people in the world (together with Jennifer Doudna).</view>
<view>•	2009 – Theodor Körner Prize for Science and Culture</view>
<view>•	2011 – The Fernström Prize for young and promising scientists</view>
<view>•	2014 – Alexander von Humboldt Professorship</view>
<view>•	2014 – The Göran Gustafsson Prize for Molecular Biology (Royal Swedish Academy of Sciences)</view>
<view>•	2014 – Dr. Paul Janssen Award for Biomedical Research (shared with Jennifer Doudna)</view>
<view>•	2014 – The Jacob Heskel Gabbay Award (shared with Feng Zhang and Jennifer Doudna)</view>
<view>•	2015 – Time 100: Pioneers[30] (shared with Jennifer Doudna)</view>
<view>•	2015 – The Breakthrough Prize in Life Sciences (shared with Jennifer Doudna)</view>
<view>•	2015 – Louis-Jeantet Prize for Medicine</view>
<view>•	2015 – The Ernst Jung Prize in Medicine</view>
<view>•	2015 – Princess of Asturias Awards (shared with Jennifer Doudna)</view>
<view>•	2015 – Gruber Foundation International Prize in Genetics (shared with Jennifer Doudna)</view>
<view>•	2015 – Carus Medal [de], from German National Academy of Science, Leopoldina</view>
<view>•	2015 – Massry Prize</view>
<view>•	2016 – Otto Warburg Medal</view>
<view>•	2016 – L’Oréal-UNESCO "For Women in Science" Award</view>
<view>•	2016 – Leibniz Prize from the German Research Foundation( Jennifer Doudna)</view>
<view>•	2016 – Tang Prize (shared with Jennifer Doudna and Feng Zhang)</view>
<view>•	2016 – HFSP Nakasone Award (jointly with Jennifer Doudna)</view>
<view>•	2016 – Knight (Chevalier) French National Order of the Legion of Honour</view>
<view>•	2016 – Meyenburg Prize</view>
<view>•	2016 – Wilhelm Exner Medal</view>
<view>•	2016 – John Scott Award</view>
<view>•	2017 – BBVA Foundation Frontiers of Knowledge Award (jointly with Jennifer Doudna and Francisco Mojica)</view>
<view>•	2017 – Japan Prize (jointly with Jennifer Doudna)</view>
<view>•	2017 – Albany Medical Center Prize (jointly with Jennifer Doudna, Luciano Marraffini, Francisco Mojica,and Feng Zhang)</view>
<view>•	2017 – Pour le Mérite</view>
<view>•	2018 – Kavli Prize in Nanoscience</view>
<view>•	2018 – Austrian Decoration for Science and Art</view>
<view>•	2018 – Bijvoet Medal of the Bijvoet Center for Biomolecular Research of Utrecht University</view>
<view>•	2018 – Harvey Prize (jointly with Jennifer Doudna and Feng Zhang)</view>
<view>•	2019 – Scheele Award of the Swedish Pharmaceutical Society</view>
<view>•	2019 – Knight Commander's Cross of the Order of Merit of the Federal Republic of Germany</view>
<view>•	2020 – Wolf Prize in Medicine (jointly with Jennifer Doudna)</view>
<view>•	2020 – Nobel Prize in Chemistry (jointly with Jennifer Doudna)</view>